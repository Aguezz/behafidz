/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package helpers;

/**
 *
 * @author aguezz
 */
public class User {
    private static int id = 1;
    private static String nama;
    private static String username;
    
    public static void setId(int id) {
        User.id = id;
    }
    
    public static void setNama(String nama) {
        User.nama = nama;
    }
    
    public static void setUsername(String username) {
        User.username = username;
    }
    
    public static int getId() {
        return User.id;
    }
    
    public static String getNama() {
        return User.nama;
    }
    
    public static String getUsername() {
        return User.username;
    }
}
