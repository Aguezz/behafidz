/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package helpers;

/**
 *
 * @author aguezz
 */
public class Surat {
    private static int noSurat;
    private static String namaSurat;
    
    public static void setNoSurat(int noSurat) {
        Surat.noSurat = noSurat;
    }
    
    public static void setNamaSurat(String namaSurat) {
        Surat.namaSurat = namaSurat;
    }
    
    public static int getNoSurat() {
        return Surat.noSurat;
    }
    
    public static String getNamaSurat() {
        return Surat.namaSurat;
    }
}
