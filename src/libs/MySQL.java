/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package libs;

/**
 *
 * @author aguezz
 */

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

public class MySQL {
    /**
     * Configuration Database
     */
    private static final String JDBC_DRIVER = "com.mysql.cj.jdbc.Driver";
    private static final String DB_URL = "jdbc:mysql://agus.website:3307/tekpro";
    private static final String DB_USER = "kelompok1";
    private static final String DB_PASS = "u!nsarayaaa";
    
    private static Connection conn;
    
    private static void checkConnection() {
        if (conn == null) {
            try {
                Class.forName(JDBC_DRIVER);

                conn = DriverManager.getConnection(DB_URL, DB_USER, DB_PASS);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
    
    public static Connection getConnection() {
        checkConnection();
        return conn;
    }

    public static ResultSet query(String query) throws SQLException {
        checkConnection();
        return conn.createStatement().executeQuery(query);
    }
    
    public static boolean execute(String query) throws SQLException {
        checkConnection();
        return conn.createStatement().execute(query);
    }
    
    public static void close() {
        try {
            conn.close();
        } catch (SQLException ex) {
            Logger.getLogger(MySQL.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    

    
}
