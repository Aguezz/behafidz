/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package behafidz.SambungAyat;

import helpers.Surat;
import helpers.User;
import java.awt.Color;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import libs.MySQL;

/**
 *
 * @author lenovo
 */
public class Kuis extends javax.swing.JFrame {
    private int posisiAyat;         // Berisi sekarang urutan ayat ke berapa
    private int posisiJawabanBenar; // Jawaban benar pertanyaan ini, 1, 2, 3 atau 4
    private int jawabanUser;        // Jawaban dari user
    private ResultSet ayat;         // Ayat yang benar
    private ResultSet ayatRandom;   // Ayat acak dari surat lain

    /**
     * Creates new form Kuis
     */
    public Kuis() {
        initComponents();
    }
    
    public void mulai() {
        this.setLabelNamaSurat();   // Mengganti nama surat pada header
        this.ambilDataAyat();       // Mengambil ayat surat yang benar
        this.ambilDataAyatRandom(); // Mengambil ayat lain secara acak
        this.next();
    }
    
    private void setLabelNamaSurat() {
        labelNamaSurat.setText("Surat " + Surat.getNamaSurat());
    }
    
    private void setLabelPertanyaan() {
        labelPertanyaan.setText("Ayat ke-" + posisiAyat + " dari surat " + Surat.getNamaSurat() + " adalah ...");
    }
            
    // Mengambil data ayat asli dan mengurutkan urutannya dengan benar
    private void ambilDataAyat() {
        try {
            String query = "SELECT * FROM ayat WHERE no_surat = %d ORDER BY no_ayat ASC";
            query = String.format(query, Surat.getNoSurat());
            ayat = MySQL.query(query);
        } catch (SQLException ex) {
            Logger.getLogger(Kuis.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    // Mengambil data ayat lain secara acak dan mengacak urutannya juga
    // Mengambil ayat sebanyak 3x jumlah ayat asli
    private void ambilDataAyatRandom() {
        try {
            // Menghitung jumlah ayat
            int jumlah = 0;
            while (ayat.next()) {
                jumlah++;
            }
            ayat.beforeFirst();

            String query = "SELECT * FROM (SELECT * FROM ayat WHERE no_surat <> %d ORDER BY no_surat DESC LIMIT %d) test ORDER BY RAND()";
            query = String.format(query, Surat.getNoSurat(), jumlah * 3);
            ayatRandom = MySQL.query(query);
        } catch (SQLException ex) {
            Logger.getLogger(Kuis.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    private int generateRandomPosition() {
        int min = 1;
        int max = 4;
        
        return (int) (Math.random() * (max - min + 1)) + min;
    }
    
    private void setPosisiJawaban(int posisi, String ayat) {
        ayat = ayat.replace("بِسْمِ اللَّهِ الرَّحْمَٰنِ الرَّحِيمِ ", "");
        switch (posisi) {
            case 1:
                pilihan1.setText(ayat);
                break;
                
            case 2:
                pilihan2.setText(ayat);
                break;
                
            case 3:
                pilihan3.setText(ayat);
                break;
                
            case 4:
                pilihan4.setText(ayat);
                break;
                
            default:
                return;
        }
    }
    
    private boolean apakahAyatTerakhir() {
        try {
            if (ayat.next()) return false;
        } catch (SQLException ex) {
            Logger.getLogger(Kuis.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return true;
    }
    
    private void berhasil() {
        try {
            this.setVisible(false);
            
            String query = "INSERT INTO riwayat (id_user, keterangan) VALUES (%d, '%s')";
            query = String.format(query, User.getId(), "Berhasil melakukan hafalan surat " + Surat.getNamaSurat());
            MySQL.execute(query);
            
            new sukses().setVisible(true);
        } catch (SQLException ex) {
            Logger.getLogger(Kuis.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    private void gagal() {
        try {
            javax.swing.JOptionPane.showMessageDialog(this, "Yah... Jawaban kamu salah :(", "Salah", javax.swing.JOptionPane.ERROR_MESSAGE);
            String query = "INSERT INTO riwayat (id_user, keterangan) VALUES (%d, '%s')";
            query = String.format(query, User.getId(), "Gagal melakukan hafalan surat " + Surat.getNamaSurat());
            MySQL.execute(query);
            this.setVisible(false);
            new layout().setVisible(true);
        } catch (SQLException ex) {
            Logger.getLogger(Kuis.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    private void next() {
        try {
            if (this.apakahAyatTerakhir()) {
                this.berhasil();
                return;
            }
            
            posisiJawabanBenar = 0;
            this.resetPilihanColor();
            buttonNext.setEnabled(false);
            posisiAyat = ayat.getInt("no_ayat");
            posisiJawabanBenar = this.generateRandomPosition();
            
            this.setLabelPertanyaan();        
            this.setPosisiJawaban(posisiJawabanBenar, ayat.getString("ayat"));
            this.generatePosisiRandomAyat();
        } catch (SQLException ex) {
            Logger.getLogger(Kuis.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    private void generatePosisiRandomAyat() {
        for (int i = 1; i <= 4; i++) {
            try {
                if (i == posisiJawabanBenar) continue;
                ayatRandom.next();
                this.setPosisiJawaban(i, ayatRandom.getString("ayat"));
            } catch (SQLException ex) {
                Logger.getLogger(Kuis.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
    
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jPanel2 = new javax.swing.JPanel();
        buttonNext = new javax.swing.JButton();
        keluarButton = new javax.swing.JButton();
        pilihan4 = new javax.swing.JButton();
        pilihan3 = new javax.swing.JButton();
        pilihan2 = new javax.swing.JButton();
        pilihan1 = new javax.swing.JButton();
        labelPertanyaan = new javax.swing.JLabel();
        labelNamaSurat = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setBackground(new java.awt.Color(0, 0, 204));

        jPanel1.setBackground(new java.awt.Color(0, 102, 153));
        jPanel1.setPreferredSize(new java.awt.Dimension(1000, 600));

        jPanel2.setPreferredSize(new java.awt.Dimension(950, 550));

        buttonNext.setBackground(new java.awt.Color(0, 102, 153));
        buttonNext.setFont(new java.awt.Font("Gentium", 1, 20)); // NOI18N
        buttonNext.setForeground(new java.awt.Color(255, 255, 255));
        buttonNext.setText("Next");
        buttonNext.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        buttonNext.setEnabled(false);
        buttonNext.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                buttonNextActionPerformed(evt);
            }
        });

        keluarButton.setBackground(new java.awt.Color(153, 153, 153));
        keluarButton.setFont(new java.awt.Font("Gentium", 1, 20)); // NOI18N
        keluarButton.setForeground(new java.awt.Color(0, 0, 0));
        keluarButton.setText("Keluar");
        keluarButton.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        keluarButton.setMaximumSize(new java.awt.Dimension(88, 34));
        keluarButton.setMinimumSize(new java.awt.Dimension(88, 34));
        keluarButton.setPreferredSize(new java.awt.Dimension(88, 34));
        keluarButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                keluarButtonActionPerformed(evt);
            }
        });

        pilihan4.setFont(new java.awt.Font("Amiri", 0, 24)); // NOI18N
        pilihan4.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        pilihan4.setFocusable(false);
        pilihan4.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                pilihan4ActionPerformed(evt);
            }
        });

        pilihan3.setFont(new java.awt.Font("Amiri", 0, 24)); // NOI18N
        pilihan3.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        pilihan3.setFocusable(false);
        pilihan3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                pilihan3ActionPerformed(evt);
            }
        });

        pilihan2.setFont(new java.awt.Font("Amiri", 0, 24)); // NOI18N
        pilihan2.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        pilihan2.setFocusable(false);
        pilihan2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                pilihan2ActionPerformed(evt);
            }
        });

        pilihan1.setFont(new java.awt.Font("Amiri", 0, 24)); // NOI18N
        pilihan1.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        pilihan1.setFocusable(false);
        pilihan1.setPreferredSize(new java.awt.Dimension(35, 10));
        pilihan1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                pilihan1ActionPerformed(evt);
            }
        });

        labelPertanyaan.setFont(new java.awt.Font("Gentium", 1, 24)); // NOI18N
        labelPertanyaan.setText("Ayat Ke-{} dari Surat {} adalah ...");

        labelNamaSurat.setFont(new java.awt.Font("Gentium", 1, 36)); // NOI18N
        labelNamaSurat.setText("Surat {}");

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(keluarButton, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(67, 67, 67)
                .addComponent(buttonNext, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(67, 67, 67))
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGap(20, 20, 20)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.CENTER)
                    .addComponent(pilihan4, javax.swing.GroupLayout.PREFERRED_SIZE, 910, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(pilihan3, javax.swing.GroupLayout.PREFERRED_SIZE, 910, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(pilihan2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(pilihan1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap(20, Short.MAX_VALUE))
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGap(356, 356, 356)
                        .addComponent(labelNamaSurat))
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGap(286, 286, 286)
                        .addComponent(labelPertanyaan)))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                .addGap(31, 31, 31)
                .addComponent(labelNamaSurat)
                .addGap(31, 31, 31)
                .addComponent(labelPertanyaan)
                .addGap(32, 32, 32)
                .addComponent(pilihan1, javax.swing.GroupLayout.PREFERRED_SIZE, 48, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(30, 30, 30)
                .addComponent(pilihan2, javax.swing.GroupLayout.PREFERRED_SIZE, 53, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(30, 30, 30)
                .addComponent(pilihan3, javax.swing.GroupLayout.PREFERRED_SIZE, 51, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 25, Short.MAX_VALUE)
                .addComponent(pilihan4, javax.swing.GroupLayout.PREFERRED_SIZE, 51, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(28, 28, 28)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(keluarButton, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(buttonNext, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(29, 29, 29))
        );

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addContainerGap(30, Short.MAX_VALUE)
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(20, 20, 20))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addContainerGap(28, Short.MAX_VALUE)
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(22, 22, 22))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
        );

        pack();
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void resetPilihanColor() {
        pilihan1.setBackground(null);
        pilihan1.setForeground(null);
        pilihan2.setBackground(null);
        pilihan2.setForeground(null);
        pilihan3.setBackground(null);
        pilihan3.setForeground(null);
        pilihan4.setBackground(null);
        pilihan4.setForeground(null);
    }
    
    private void setPilihanColor(javax.swing.JButton pilihan) {
        this.resetPilihanColor();
        buttonNext.setEnabled(true);
        
        pilihan.setBackground(Color.decode("#035aa6"));
        pilihan.setForeground(Color.white);
    }
    
    private void keluarButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_keluarButtonActionPerformed
        int dialogButton = javax.swing.JOptionPane.YES_NO_OPTION;
        int dialogResult = javax.swing.JOptionPane.showConfirmDialog(this, "Apakah kamu yakin ingin keluar?", "Peringatan", dialogButton);
        if(dialogResult == javax.swing.JOptionPane.YES_OPTION){
            this.setVisible(false);
            new layout().setVisible(true);
        }
    }//GEN-LAST:event_keluarButtonActionPerformed

    private void pilihan1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_pilihan1ActionPerformed
        this.jawabanUser = 1;
        this.setPilihanColor(pilihan1);
    }//GEN-LAST:event_pilihan1ActionPerformed

    private void pilihan2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_pilihan2ActionPerformed
        this.jawabanUser = 2;
        this.setPilihanColor(pilihan2);
    }//GEN-LAST:event_pilihan2ActionPerformed

    private void pilihan4ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_pilihan4ActionPerformed
        this.jawabanUser = 4;
        this.setPilihanColor(pilihan4);
    }//GEN-LAST:event_pilihan4ActionPerformed

    private void buttonNextActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_buttonNextActionPerformed
        if (this.posisiJawabanBenar == this.jawabanUser) this.next();
        else this.gagal();
    }//GEN-LAST:event_buttonNextActionPerformed

    private void pilihan3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_pilihan3ActionPerformed
        this.jawabanUser = 3;
        this.setPilihanColor(pilihan3);
    }//GEN-LAST:event_pilihan3ActionPerformed

    private void jButton3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton3ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jButton3ActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Kuis.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Kuis.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Kuis.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Kuis.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Kuis().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton buttonNext;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JButton keluarButton;
    private javax.swing.JLabel labelNamaSurat;
    private javax.swing.JLabel labelPertanyaan;
    private javax.swing.JButton pilihan1;
    private javax.swing.JButton pilihan2;
    private javax.swing.JButton pilihan3;
    private javax.swing.JButton pilihan4;
    // End of variables declaration//GEN-END:variables
}
